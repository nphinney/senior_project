# SENIOR_PROJECT


## File Descriptions
- `test_replay.c` - This code can be ran after running `capture.sh` to replay commands sent over USB. The code has to be pointed at the file to read from. 
- `pod_edit.c` - This is the start of actually controlling the device with user input. It takes in an "intialization capture" and replays it, similar to `test_replay.c`, and then it goes into a loop waiting for user input to specifiy what effect the user wants to turn on or off. This code is still in production and needs to be finished in order to work.
- `test_change.py` - This code was used at the start of the project but is now not used for anything. I have left it just for refrence of what I have done. Some code within it has been changed. 
- `./capturedTestFiles/*` - These are just some packet capture files that were created by `capture.sh`.
- `capture.sh` - This runs a `tshark` command that captures a usb connection on a sprecific device address and then puts the captured data into a file specified within the script.
- `DEVICE_INFO.md` - An outline of the device descriptors.

-----------------------------------------
## Wiki pages
- `findings during research` - This wiki page contains some of the things I have found along the way as well as some of the commands from the podgo edit software broken down.
