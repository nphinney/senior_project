# POD GO
--------------------------
## IDs
- Vendor ID - 0x0e41
- Product ID - 0x424b
- Connection ID - (varies upon connection) 8

## Configuration Descriptor
- Endpoint Address : `0x81`
    - Endpoint Direction: `IN`
    - Transfer Type: `Bulk`
    - Max packet size: `512 bytes`
    - Interface Class: `Vendor Specific Class`
        - Subclass: `None` 
    - Interface ID: `0`

- Endpoint Address : `0x01`
    - Endpoint Direction: `OUT`
    - Transfer Type: `Bulk`
    - Max packet size: `512 bytes`
    - Interface Class: `Vendor Specific Class`
        - Subclass: `None` 
    - Interface ID: `0`

- Endpoint Address : `0x03`
    - Endpoint Direction: `OUT`
    - Transfer Type: `Isochronous`
    - Max packet size: `112 bytes`
    - Interface Class: `Audio`
        - Subclass: `Streaming`
    - Interface ID: `2`

- Endpoint Address : `0x83`
    - Endpoint Direction: `IN`
    - Transfer Type: `Isochronous`
    - Max packet size: `112 bytes`
    - Interface Class: `Audio`
        - Subclass: `Streaming`
    - Interface ID: `3`

- Endpoint Address : `0x02`
    - Endpoint Direction: `OUT`
    - Transfer Type: `Bulk`
    - Max packet size: `512 bytes`
    - Interface Class: `Audio`
        - Subclass: `MIDI Streaming`
    - Interface ID: `4`

- Endpoint Address : `0x82`
    - Endpoint Direction: `IN`
    - Transfer Type: `Bulk`
    - Max packet size: `512 bytes`
    - Interface Class: `Audio`
        - Subclass: `MIDI Streaming`
    - Interface ID: `4`