#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <libusb-1.0/libusb.h>

#pragma pack(1)

//PodGo Setup
#define VENDOR_ID       0x0e41
#define PRODUCT_ID      0x424b
#define BULK_OUT_EP     0x01
#define BULK_IN_EP      0x81
#define IFACE       0
#define TRANSFER_SIZE   512
#define WRITE_TRUE      1
#define READ_TRUE       1
#define WRITE_FALSE     0
#define READ_FALSE      0

//Keyboard setup
#define KEYVID          0x1038
#define KEYPID          0x1622

struct StandBy 
{
    uint32_t header;             // 0x08 0x00 0x00 0x18
    u_int8_t flip1;              // 0x##
    u_int8_t constant1;          // 0x10
    u_int8_t flip2;              // 0x##
    u_int16_t constant2;         // 0x03 0x00
    u_int8_t increment_flip1;    // 0x##
    u_int16_t constant3;         // 0x00 0x10
    u_int8_t flip3;              // 0x##
    u_int8_t flip4;              // 0x##
    u_int16_t padding;           // 0x00 0x00
};
struct ToggleEffect
{
    uint64_t header;             // 0x10 0x00 0x00 0x18 0x80 0x10 0xed 0x03
    uint8_t padding1;            // 0x00
    uint8_t time;                // 0x## 0x00  -- seconds from start. Till ff, then reset to 00
    uint16_t constant1;          // 0x00 0x04
    uint16_t var1;               // 0x## 0x##  -- starts as 0xd1 0x20 or 0x20d1, then plus 17 each call 
    uint64_t constant2;          // 0x00 0x00 0x01 0x00 0x06 0x00 0x0d 0x00
    uint32_t constant3;          // 0x00 0x00 0x83 0x66
    uint16_t constant4;          // 0xcd 0x03
    uint8_t count_var;           // 0x##       -- always starts at ef, then ++ 
    uint32_t constant5;          // 0x64 0x29 0x65 0x82
    uint8_t constant6;           // 0x62 
    uint8_t effect_index;        // 0x0#       -- 0 through 9
    uint8_t constant7;           // 0x3b
    uint16_t switch_var;         // 0xc# 0x00  -- 3 is on, 2 is off
    uint16_t padding2;           // 0x00 0x00
};

int effect_index = -1;


int char_to_int(char c)
{
    if (c >= '0' && c <= '9') {
        return c - '0';
    } else if (c >= 'A' && c <= 'F') {
        return c - 'A' + 10;
    } else if (c >= 'a' && c <= 'f') {
        return c - 'a' + 10;
    } else {
        return -1;
    }
}


unsigned char * split_hex_string(char * _cmd)
{
    int command_length = strlen((char*)_cmd);
    //create and set buffer size
    unsigned char * buffer = malloc( command_length );
    //set all of buffer to 0s
    memset(buffer, 0, command_length);

    //get length of string
    //loop through _cmd (hex string) and set them in buffer
    for (int i = 0; i < command_length; i++) {
        char c1 = _cmd[2*i];
        char c2 = _cmd[2*i+1];
        buffer[i] = (char_to_int(c1) << 4) + char_to_int(c2);
    }
    
    return buffer;
}


int write_to_device( libusb_device_handle * _device_handle, unsigned char * _cmd, int _timeout, int write_length )
{
    //used to keep track of how many bytes sent
    int bytes_sent = 0;   

    //Send the bulk transfer
    int result = libusb_bulk_transfer( _device_handle, BULK_OUT_EP, _cmd, write_length, &bytes_sent, _timeout);
    // printf( "Write result : %i, Bytes sent: %u\n", result, bytes_sent );

    if( result < 0 )
    {
        printf( "Error sending interrupt transfer: %s\n", libusb_error_name( result ) );
    }

    return result;
}


int read_from_device( libusb_device_handle * _device_handle, unsigned char * _read_str, int _timeout )
{
    int bytes_read = 0;

    unsigned char buffer[ TRANSFER_SIZE ];

    memset( buffer, 0, TRANSFER_SIZE );

    int result = libusb_bulk_transfer( _device_handle,  BULK_IN_EP, buffer, TRANSFER_SIZE, &bytes_read, _timeout);
    // printf("Read result: %i, bytes read: %u\n", result, bytes_read);

    if ( result < 0 )
    {
        printf( "Error reading interrupt transfer: %s\n", libusb_error_name(result) );
        return result;
    }
    
    memcpy( _read_str, &buffer[0], TRANSFER_SIZE);
    return result;
}

int call_connection( libusb_device_handle *_device_handle, char * data_string, unsigned char * read_str, int write_bool, int read_bool )
{   
    int result;
    int timeout = 1000;

    unsigned char * cmd;
    int write_length = strlen(data_string)/2;
    memset(read_str, 0, TRANSFER_SIZE);
    cmd = (unsigned char * ) malloc(write_length);
    cmd = split_hex_string(data_string);

    if ( write_bool )
    {
        result = write_to_device( _device_handle, cmd, timeout, write_length );
        if ( result < 0 )
        {
            printf( "Error in init: %s\n", libusb_error_name ( result ) );
            exit( -4 );
        }   
    }

    if ( read_bool )
    {
        result = read_from_device(_device_handle, read_str, timeout);
        if ( result < 0 )
        {
            printf( "Error in init: %s\n", libusb_error_name ( result ) );
            exit( -5 );
        }
    }

    free(cmd);
    return 0;
}

void * power_effect(void * thread_id)
{
    while( 1 )
    {
        printf("Enter an effect index: ");
        scanf("%i", &effect_index);
        if( effect_index > -1 && effect_index < 10 )
        {
            pthread_exit(NULL);
        }
        else
        {
            effect_index = -1;
        }
    }
    pthread_exit(NULL);
}

int connection(libusb_device_handle *_device_handle)
{

    unsigned char read_str[TRANSFER_SIZE];
    int timeout = 1000;
    int result;
    int chunk_length;
    FILE * stream;
    char * line = NULL;
    size_t len = 0;
    ssize_t nread;

    struct StandBy standBy;
    standBy.header = 0x18000008;
    standBy.flip1 = 0x02;
    standBy.constant1 = 0x10;
    standBy.flip2 = 0xf0;
    standBy.constant2 = 0x0003;
    standBy.increment_flip1 = 0x04;
    standBy.constant3 = 0x1000;
    standBy.flip3 = 0x09;
    standBy.flip4 = 0x10;
    standBy.padding = 0x0000;
    unsigned char increment_array[] = {0x04, 0x16, 0x17};

    struct ToggleEffect toggleEffect;
    toggleEffect.header = 0x03ed10801800001d;
    toggleEffect.padding1 = 0x00;
    toggleEffect.time = 0x001e;
    toggleEffect.constant1 = 0x0400;
    toggleEffect.var1 = 0x20d1;
    toggleEffect.constant2 = 0x000d000600010000;
    toggleEffect.constant3 = 0x66830000;
    toggleEffect.constant4 = 0x03cd;
    toggleEffect.count_var = 0xed;
    toggleEffect.constant5 = 0x82652964;
    toggleEffect.constant6 = 0x62;
    toggleEffect.effect_index = 0x03;
    toggleEffect.constant7 = 0x3b;
    toggleEffect.switch_var = 0x00c3;
    toggleEffect.padding2 = 0x00;
    

    stream = fopen("./capturedTestFiles/init_connection.txt", "r");
    if ( stream == NULL )
    {
        perror("fopen");
        exit(EXIT_FAILURE);
    }
    // Inti the connection with the board
    while((nread = getline(&line, &len, stream)) != -1){
        chunk_length = nread - 2;
        if ( line[chunk_length] == '0' ){
            char parsed_line[chunk_length];
            memcpy(parsed_line, line, chunk_length);
            result = call_connection(_device_handle, parsed_line, read_str, WRITE_TRUE, READ_FALSE);
        }
        else{
            char parsed_line[chunk_length];
            memcpy(parsed_line, line, chunk_length);
            result = call_connection(_device_handle, parsed_line, read_str, WRITE_FALSE, READ_TRUE);
        }
    }

    pthread_t pthread_id;
    pthread_create(&pthread_id, NULL, power_effect, NULL);
    // Standby loop
    while ( 1 )
    {   
        // sleep(0);
        result = read_from_device(_device_handle, read_str, timeout);
        result = write_to_device(_device_handle, (unsigned char *)&standBy, timeout, sizeof(standBy));

        if(standBy.flip1 == 0x02){
            standBy.flip1 = 0x80;
            standBy.flip2 = 0xed;
            standBy.flip3 = 0xcd;
            standBy.flip4 = 0x1e;
            standBy.increment_flip1 = increment_array[1]; 
        }
        else if(standBy.flip1 == 0x80){
            standBy.flip1 = 0x01;
            standBy.flip2 = 0xef;
            standBy.flip3 = 0xb9;
            standBy.flip4 = 0x1d;
            standBy.increment_flip1 = increment_array[2]; 
        }
        else if(standBy.flip1 == 0x01){
            for( int i = 0; i < 3; i++)
            {
                increment_array[i]++;
            }
            standBy.flip1 = 0x02;
            standBy.flip2 = 0xf0;
            standBy.flip3 = 0x09;
            standBy.flip4 = 0x10;
            standBy.increment_flip1 = increment_array[0];

            if( effect_index != -1 )
            {
                toggleEffect.time = increment_array[2];
                result = write_to_device(_device_handle, (unsigned char *)&toggleEffect, timeout, sizeof(toggleEffect));
                result = read_from_device(_device_handle, read_str, timeout);
                result = write_to_device(_device_handle, (unsigned char *)&standBy, timeout, sizeof(standBy));
                result = read_from_device(_device_handle, read_str, timeout);
                effect_index = -1;
            }
        }
        else
        {
            printf("Error changing standby values");
        }
    }
    

    free(line);
    fclose(stream);
    return result;
}


int main() 
{
    struct libusb_device_handle *device_handle = NULL;

    // Init of libusb
    int result = libusb_init( NULL );
    if ( result < 0 )
    {
        printf( "Error in init: %s\n", libusb_error_name ( result ) );
        exit( -1 );
    }

    libusb_set_option( NULL, LIBUSB_OPTION_LOG_LEVEL, LIBUSB_LOG_LEVEL_WARNING);

    // Open device that matches with the VID and the PID

    device_handle = libusb_open_device_with_vid_pid(NULL, VENDOR_ID, PRODUCT_ID);
    if ( !device_handle )
    {
        printf( "Error finding the USB device\n");
        libusb_exit( NULL );
        exit( -2 );
    }
    libusb_set_auto_detach_kernel_driver( device_handle, 1 );

    result = libusb_claim_interface( device_handle, IFACE );
    if ( result < 0 )
    {
        printf( "Error claiming the interface: %s\n", libusb_error_name( result ) );
        if ( device_handle )
        {
            libusb_close( device_handle );
        }
        libusb_exit( NULL );
        exit (-3 );
    }

    //A function that calls the read and write functions
    connection(device_handle);

    //Close and release device
    libusb_release_interface( device_handle, IFACE );
    libusb_close( device_handle );

    libusb_exit( NULL );

    return 0;
}