#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <libusb-1.0/libusb.h>

//PodGo Setup
#define VENDOR_ID       0x0e41
#define PRODUCT_ID      0x424b
#define BULK_OUT_EP     0x01
#define BULK_IN_EP      0x81
#define IFACE       0
#define TRANSFER_SIZE   512
#define WRITE_TRUE      1
#define READ_TRUE       1
#define WRITE_FALSE     0
#define READ_FALSE      0

//Keyboard setup
#define KEYVID          0x1038
#define KEYPID          0x1622


int char_to_int(char c)
{
    if (c >= '0' && c <= '9') {
        return c - '0';
    } else if (c >= 'A' && c <= 'F') {
        return c - 'A' + 10;
    } else if (c >= 'a' && c <= 'f') {
        return c - 'a' + 10;
    } else {
        return -1;
    }
}


unsigned char * split_hex_string(char * _cmd)
{
    int command_length = strlen((char*)_cmd);
    //create and set buffer size
    unsigned char * buffer = malloc( command_length );
    //set all of buffer to 0s
    memset(buffer, 0, command_length);

    //get length of string
    //loop through _cmd (hex string) and set them in buffer
    for (int i = 0; i < command_length; i++) {
        char c1 = _cmd[2*i];
        char c2 = _cmd[2*i+1];
        buffer[i] = (char_to_int(c1) << 4) + char_to_int(c2);
    }
    
    return buffer;
}


int write_to_device( libusb_device_handle * _device_handle, unsigned char * _cmd, int _timeout, int write_length )
{
    //used to keep track of how many bytes sent
    int bytes_sent = 0;   

    //Send the bulk transfer
    int result = libusb_bulk_transfer( _device_handle, BULK_OUT_EP, _cmd, write_length, &bytes_sent, _timeout);
    printf( "Write result : %i, Bytes sent: %u\n", result, bytes_sent );

    if( result < 0 )
    {
        printf( "Error sending interrupt transfer: %s\n", libusb_error_name( result ) );
    }

    return result;
}


int read_from_device( libusb_device_handle * _device_handle, unsigned char * _read_str, int _timeout )
{
    int bytes_read = 0;

    unsigned char buffer[ TRANSFER_SIZE ];

    memset( buffer, 0, TRANSFER_SIZE );

    int result = libusb_bulk_transfer( _device_handle,  BULK_IN_EP, buffer, TRANSFER_SIZE, &bytes_read, _timeout);
    printf("Read result: %i, bytes read: %u\n", result, bytes_read);

    if ( result < 0 )
    {
        printf( "Error reading interrupt transfer: %s\n", libusb_error_name(result) );
        return result;
    }
    
    memcpy( _read_str, &buffer[0], TRANSFER_SIZE);
    return result;
}


int call_connection( libusb_device_handle *_device_handle, char * data_string, unsigned char * read_str, int write_bool, int read_bool )
{   
    int result;
    int timeout = 1000;

    unsigned char * cmd;
    int write_length = strlen(data_string)/2;
    memset(read_str, 0, TRANSFER_SIZE);
    cmd = (unsigned char * ) malloc(write_length);
    cmd = split_hex_string(data_string);

    if ( write_bool )
    {
        result = write_to_device( _device_handle, cmd, timeout, write_length );
        if ( result < 0 )
        {
            printf( "Error in init: %s\n", libusb_error_name ( result ) );
            exit( -4 );
        }   
    }

    if ( read_bool )
    {
        result = read_from_device(_device_handle, read_str, timeout);
        if ( result < 0 )
        {
            printf( "Error in init: %s\n", libusb_error_name ( result ) );
            exit( -5 );
        }
    }

    free(cmd);
    return 0;
}


int handle_connection(libusb_device_handle *_device_handle)
{
    unsigned char read_str[TRANSFER_SIZE];
    int result;
    int chunk_length;
    FILE * stream;
    char * line = NULL;
    size_t len = 0;
    ssize_t nread;

    // stream = fopen("./packet_capture_one.txt", "r");
    stream = fopen("./capturedTestFiles/packet_capture.txt", "r");
    if ( stream == NULL )
    {
        perror("fopen");
        exit(EXIT_FAILURE);
    }

    while((nread = getline(&line, &len, stream)) != -1){
        chunk_length = nread - 2;
        if ( line[chunk_length] == '0' ){
            char parsed_line[chunk_length];
            memcpy(parsed_line, line, chunk_length);
            result = call_connection(_device_handle, parsed_line, read_str, WRITE_TRUE, READ_FALSE);
        }
        else{
            char parsed_line[chunk_length];
            memcpy(parsed_line, line, chunk_length);
            result = call_connection(_device_handle, parsed_line, read_str, WRITE_FALSE, READ_TRUE);
        }
    }
    free(line);
    fclose(stream);
    return result;
}


int main(int argc, char *argv[]) 
{
    struct libusb_device_handle *device_handle = NULL;

    // Init of libusb
    int result = libusb_init( NULL );
    if ( result < 0 )
    {
        printf( "Error in init: %s\n", libusb_error_name ( result ) );
        exit( -1 );
    }

    libusb_set_option( NULL, LIBUSB_OPTION_LOG_LEVEL, LIBUSB_LOG_LEVEL_WARNING);

    // Open device that matches with the VID and the PID

    device_handle = libusb_open_device_with_vid_pid(NULL, VENDOR_ID, PRODUCT_ID);
    if ( !device_handle )
    {
        printf( "Error finding the USB device\n");
        libusb_exit( NULL );
        exit( -2 );
    }
    libusb_set_auto_detach_kernel_driver( device_handle, 1 );

    result = libusb_claim_interface( device_handle, IFACE );
    if ( result < 0 )
    {
        printf( "Error claiming the interface: %s\n", libusb_error_name( result ) );
        if ( device_handle )
        {
            libusb_close( device_handle );
        }
        libusb_exit( NULL );
        exit (-3 );
    }

    //A function that calls the read and write functions
    handle_connection(device_handle);

    //Close and release device
    libusb_release_interface( device_handle, IFACE );
    libusb_close( device_handle );

    libusb_exit( NULL );

    return 0;
}